// Tìm sô nguyên dương nhỏ nhất sao cho: 1 + 2 + 3 + ... + n > 10000
function minNumber(){
    var tong = 0;
    var i = 0;
    for(; tong <= 10000; i++){
        tong += i;
    }
    document.getElementById("result-1").innerHTML = `Số nguyên dương nhỏ nhất: ${i-1}`
}

// Viết vào chương trình nhập 2 số x, n tính tổng: S(n) = x + x^2 + ... + x^n
function tongS(){
    x = document.getElementById("number-x").value*1;
    n = document.getElementById("number-n").value*1;
    var S = 0;
    for(var i=0; i<n; i++){
        S += Math.pow(x,i+1);
    }
    document.getElementById("result-2").style.padding = "10px";
    document.getElementById("result-2").innerHTML = `${S}`;
}

// Nhập vào n. Tính giai thừa 1*2*...*n
function giaiThua(){
    n = document.getElementById("number").value*1;
    var result = 1;
    if(n<0){
        alert("Số không chính xác, bạn vui lòng nhập lại!!!");
        return;
    }
    for(var i=1; i<=n; i++){
        result *= i;
    }
    document.getElementById("result-3").style.padding = "10px";
    document.getElementById("result-3").innerHTML = `${result}`;
}

// Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div. Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì background màu xanh
function clickBackground(){
    contentHTML = "";
    for(var i=0; i<10; i++){
        var content = "";
        if((i+1)%2 == 0){
            content = `<div class="bg-danger">Div chẵn ${i+1}</div>`;
        }
        else{
            content = `<div class="bg-primary">Div lẻ ${i+1}</div>`;
        }
        contentHTML += content;
    }
    document.getElementById("result-4").innerHTML = contentHTML;
}

// Viết chương trình có một ô input, một button. Khi click vào button thì in ra các số nguyên tố từ 1 đến giá trị của ô input
function checkPrime(n){
    if(n<=1){
        return false;
    }
    for(var i=2; i<=n/2; i++){
        if(n%i == 0) return false;
    }
    return true;
}
function clickPrime(){
    var number = document.getElementById("number-5").value*1;
    console.log(number);
    var contentHTML = "";
    for(var i=1; i<=number; i++){
        if(checkPrime(i)==true){
            var content = `<span class="mr-2">${i}</span>`;
            contentHTML += content;
        }
    }
    document.getElementById("result-5").style.padding = "10px";
    document.getElementById("result-5").innerHTML = `Liệt kê các số nguyên tố không vượt quá n: ` + contentHTML;
}

minNumber();